package git.ehcruz.easy.doc.web.controller;

import git.ehcruz.easy.doc.domain.Especialidade;
import git.ehcruz.easy.doc.service.EspecialidadeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "especialidade")
public class EspecialidadeController {

    @Autowired
    private EspecialidadeService especialidadeService;

    @GetMapping(value = {"", "/"})
    public String abrir(Especialidade especialidade) {
        return "especialidade/especialidade";
    }
}
