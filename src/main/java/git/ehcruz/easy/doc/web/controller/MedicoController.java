package git.ehcruz.easy.doc.web.controller;

import git.ehcruz.easy.doc.domain.Medico;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "medicos")
public class MedicoController {

    @GetMapping(value = "/dados")
    public String cadastroPorMedico(Medico medico, ModelMap model) {
        return "medico/cadastro";
    }
}
