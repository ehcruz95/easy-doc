package git.ehcruz.easy.doc.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/login-error")
    public String loginError(ModelMap model) {
        model.addAttribute("alerta", "erro");
        model.addAttribute("titulo", "Credenciais invalidas!");
        model.addAttribute("texto", "Login ou senha incorretos.");
        model.addAttribute("subtexto", "Acesso permitido apenas para usuários já ativados.");
        return "login";
    }
}
