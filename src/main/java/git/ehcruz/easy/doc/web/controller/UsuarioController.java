package git.ehcruz.easy.doc.web.controller;

import git.ehcruz.easy.doc.domain.Usuario;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "u")
public class UsuarioController {

    @GetMapping(value = "/novo/cadastro/usuario")
    public String cadastroPorAdminParaAdminMedicoUsuario(Usuario usuario) {
        return "usuario/cadastro";
    }
}
