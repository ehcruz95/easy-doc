package git.ehcruz.easy.doc.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
public class HomeController {

	// abrir pagina home
	@GetMapping("/")
	public String home() {
		return "home";
	}

	@GetMapping(value = "/acesso-negado")
	public String acessoNegado(ModelMap model, HttpServletResponse response) {
		model.addAttribute("status", response.getStatus());
		model.addAttribute("error", "Acesso negado!");
		model.addAttribute("message", "Você não possui permisão de acesso!");
		return "error";
	}
}
