package git.ehcruz.easy.doc.configuration;

import git.ehcruz.easy.doc.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private UsuarioService usuarioService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //Acessos públicos
                .antMatchers("/js/**", "/webjars/**", "/css/**", "/image/**").permitAll()
                .antMatchers("/").permitAll()
                //Acessos admin
                .antMatchers("/u/**").hasAuthority("ADMIN")

                //Acessos medico
                .antMatchers("/medicos/**").hasAuthority("MEDICO")

                // Login
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").defaultSuccessUrl("/", true).failureUrl("/login-error").permitAll()
                .and()
                .logout().logoutSuccessUrl("/")
                .and()
        .exceptionHandling()
        .accessDeniedPage("/acesso-negado");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.usuarioService).passwordEncoder(new BCryptPasswordEncoder());
    }
}
