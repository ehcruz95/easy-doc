package git.ehcruz.easy.doc.repository;

import git.ehcruz.easy.doc.domain.Especialidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspecialidadeRepository extends JpaRepository<Long, Especialidade> {
}
