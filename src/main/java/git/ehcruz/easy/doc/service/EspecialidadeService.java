package git.ehcruz.easy.doc.service;

import git.ehcruz.easy.doc.domain.Especialidade;

public interface EspecialidadeService {

    void salvar(Especialidade especialidade);
}
