package git.ehcruz.easy.doc.service;

import git.ehcruz.easy.doc.domain.Perfil;
import git.ehcruz.easy.doc.domain.Usuario;
import git.ehcruz.easy.doc.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Override
    public Usuario findByEmail(String email) {
        return this.usuarioRepository.findByEmail(email);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Usuario usuario = this.findByEmail(s);
        return new User(
                usuario.getEmail(),
                usuario.getSenha(),
                AuthorityUtils.createAuthorityList(this.getPerfis(usuario.getPerfis()))
        );
    }

    private String[] getPerfis(List<Perfil> perfis) {
        String[] arr = new String[perfis.size()];
        for (int i = 0; i < perfis.size(); i++) {
            arr[i] = perfis.get(i).getDesc();
        }
        return arr;
    }
}
