package git.ehcruz.easy.doc.service;

import git.ehcruz.easy.doc.domain.Usuario;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UsuarioService extends UserDetailsService {

    Usuario findByEmail(String email);
}
